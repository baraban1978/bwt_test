# BWT QUEST. #
http://bwt-quest.xp3.biz/

## Данный репозиторий содержит материалы по реализации тестового задания: ##

###
- создать аккаунт на bitbucket (если его нет)
- создать приватный проект  bwt_test
- все изменения в коде вести через git.
- создать 4 страницы с одной точкой входа.
- при создании страниц использовать Model-view-controller.
* 	страница 1 - форма регистрации. Поля: имя, фамилия, email, пол, день рождения password(пол и день рождения - необязательные поля. валидация должна быть и на стороне клиента, и на стороне сервера)
* 	страница 2 - отображать погоду на сегодня в городе Запорожье (только зарегистрированные пользователи имеют доступ). Данные парсить по этойссылке http://www.gismeteo.ua/city/daily/5093/
* 	страница 3 - форма обратной связи. поля: имя, email, сообщение (все поля обязательные, валидация должна быть и на стороне клиента, и на сторонесервера). Также добавить капчу.
* 	страница 4 - отображать список фидбеков, оставленных на странице 3.(только зарегистрированные пользователи имеют доступ)
- для верстки использовать framework bootstrap 3.
- для парсинга погоды можно использовать пакет guzzle ( https://github.com/guzzle/guzzle )
- создать ER-диаграму структуры созданной базы данных. Так же положить ее в git.
###

-----------------------------------------------------------------------------------------------------------------


```
#!php


## Структура проекта ##


.htaccess
README.md
config.php
index.php
mysql/
├── ER_BD.jpg
└── bwt_quest.sql
views/
├── css/
│   ├── bootstrap.min.css
│   ├── my_style.css
│   └── origin_18fe88d4.css
├── js/
│   ├── script.js
│   ├── jquery-3.1.0.min.js
│   ├── bootstrap.min.js
│   └── validator.min.js
├── img/
│   ├── background.png
│   ├── logo.jpg
│   ├── logo1.jpg
│   └── logo2.jpg
├── fonts/
│   ├── glyphicons-halflings-regular.eot
│   ├── glyphicons-halflings-regular.svg
│   ├── glyphicons-halflings-regular.ttf
│   ├── glyphicons-halflings-regular.wolf
│   ├── glyphicons-halflings-regular.wolf2
│   └── oswald.ttf
├── phpmailer/
│   ├── PHPMailerAutoload.php
│   ├── class.phpmailer.php
│   ├── class.phpmaileroauth
│   ├── class.phpmaileroauthgoogle
│   ├── class.pop3
│   └── class.smtp 
├── Page1.html
├── Page2.html
├── Page3.html
├── Page4.html
├── captcha.php
├── message.txt
├── process.php
└── 

```

-----------------------------------------------------------------------------------------------------------------

### Используемые файлы и их назначение:
* bootstrap.min.css, bootstrap.min.js и файлы шрифтов в директории fonts (платформа Twitter Bootstrap 3);
* jquery-3.1.0.min.js (библиотека jQuery);

## index.php - Единая точка входа##

## Page1.php - Форма регистрации ##
* Page1.php  (страница, содержащая html код. На странице есть две формы которые активируют php файлы.;
* form1 - "views/php/login.php", form - "views/php/register.php".
* есть еще невидимая форма которая получает статус видимости после авторизации пользователя.
* Соответственно форма регистрации "form" и форма активации "form1" становятся невидимыми.);
* login.php (php код, отвечает за активацию пользователя);
* register.php (php код, отвечает за регистрацию пользователя);
* logout.php (php код, отвечает за выход пользователя);
* validator.min.js (скрипт, обеспечивающий валидацию формы регистрации в браузере);



## Page2.php - Погода в Запорожье ##
* Page2.php  (страница, содержащая html код.);
* simple_html_dom.php (php библиотека для парсинга данных);
* ИСТОЧНИК: https://sourceforge.net/projects/simplehtmldom/files/



## Page3.php -  Форма обратной связи на php, html и bootstrap ##
* ИСТОЧНИК: https://itchief.ru/lessons/bootstrap-3/creating-feedback-form-using-bootstrap-php-and-ajax
* background.png (фон, на который будет накладываться шрифт при создании капчи);
* captcha.php (php код, генерирующий капчу);
* Page3.php (страница, содержащая html код формы обратной связи);
* message.txt (текстовый файл, в который при необходимости можно записывать информацию с формы обратной связи);
* oswald.ttf (шрифт, с помощью которого будем писать код капчи на изображении);
* script.js (скрипт, обеспечивающий функционирование формы обратной связи в браузере);
* файлы, расположенные в директории phpmailer (php библиотека для отправки писем);
* 		- PHPMailer SPL autoloader.
* 		- PHP Version 5
* 		- @package PHPMailer
* 		- @link https://github.com/PHPMailer/PHPMailer/ The PHPMailer GitHub project
* process.php (php код, который будет обрабатывать данные формы на стороне сервера).



## Page4.php -  Таблица сообщений ##
* Page4.php  (страница, содержащая html код.);


###
-----------------------------------------------------------------------------------------------------------------