-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Время создания: Янв 16 2017 г., 07:44
-- Версия сервера: 5.5.39
-- Версия PHP: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `bwt_quest`
--

-- --------------------------------------------------------

--
-- Структура таблицы `feedback`
--
-- Создание: Янв 09 2017 г., 22:55
-- Последнее обновление: Янв 16 2017 г., 06:43
--

CREATE TABLE IF NOT EXISTS `feedback` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(20) NOT NULL,
  `email` varchar(32) NOT NULL,
  `text` text NOT NULL,
  `data` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Дамп данных таблицы `feedback`
--

INSERT INTO `feedback` (`id`, `name`, `email`, `text`, `data`) VALUES
(1, 'igor', 'kb_sk@mail.ru', 'Привет!\r\nКак дела :)', '2017-01-15 14:43:52'),
(10, 'Игорь', 'kb_sk@mail.ru.zp', 'Вывод сообщений отсортированный по возрастанию.', '2017-01-16 06:43:25'),
(8, 'Саша', 'cv@mail.ru', '&lt;h1&gt;Проверка html в сообщении&lt;h1&gt;', '2017-01-16 06:15:13'),
(9, 'Рома', 'sd@mail.ru', 'Все  ок!', '2017-01-16 06:18:02'),
(7, 'Петя', 'kb_sk@mail.ru.ua', 'Привет! Привет!', '2017-01-16 05:57:23');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--
-- Создание: Янв 12 2017 г., 20:29
-- Последнее обновление: Янв 16 2017 г., 06:10
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(20) NOT NULL COMMENT 'Имя',
  `surname` varchar(20) NOT NULL COMMENT 'Фамилия',
  `day` varchar(2) NOT NULL COMMENT 'День',
  `month` varchar(2) NOT NULL COMMENT 'Месяц',
  `year` varchar(4) NOT NULL COMMENT 'Год',
  `email` varchar(32) NOT NULL COMMENT 'Адрес',
  `password` varchar(32) NOT NULL COMMENT 'Пароль',
  `sex` varchar(1) NOT NULL COMMENT 'Пол',
  `user_hash` varchar(32) NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `name`, `surname`, `day`, `month`, `year`, `email`, `password`, `sex`, `user_hash`) VALUES
(4, 'igor', 'baraban', '13', '06', '1978', 'kb_sk@mail.ru.zp', 'd9b1d7db4cd6e70935368a1efb10e377', '1', '1f319af3343ebd18419884938764209c');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `feedback`
--
ALTER TABLE `feedback`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `feedback`
--
ALTER TABLE `feedback`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
