<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Quest - bwt</title>

    <!-- Bootstrap -->
    <link href="views/css/bootstrap.min.css" rel="stylesheet">
    <link href="views/css/my_style.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
    <body>
        <div class="karkaswrapper">
        <div class="karkas">
            <!-- heder -->
        <div class="header">
            <div class="col-xs-3 logo">
                <a href="http://www.groupbwt.com/quest/">
                    <img src="views/img/logo1.png" alt="логотип" title="logo"
                        onmouseup="this.src='views/img/logo2.png'" 
                        onmousedown="this.src='views/img/logo1.png'"
			onmouseout="this.src='views/img/logo1.png'" 
                        onmouseover="this.src='views/img/logo2.png'">
                </a>
            </div>
            <div class="col-xs-6 page_name">
                <p>PAGE 3</p>
                <p>Форма обратной связи</p>
                
            </div>
            <div class="col-xs-3">
            <div class="pass_form">
                <form action="views/php/logout.php" method="post">
                   <?php
                   echo '<pre>';
                   echo 'Привет';
                   echo '<br>';
                    print_r($_COOKIE['name']);
                   echo '<br>'; 
                    print_r($_COOKIE['surname']);
                   echo '</pre>';
                   ?>
                   <input type="submit" class="btn btn-danger btn-xs" name="submit2" value="Выход"/>
                </form>
            </div>
            </div>
	</div>
            <!-- menu -->
        <div class="menu_wrapper">
                    <div class="menu-main">
                        <ul class="menu">
                            <li><a href="index.php?option=Page1">PAGE1</a></li>
                            <li><a href="index.php?option=Page2">PAGE2</a></li>
                            <li><a href="index.php?option=Page3">PAGE3</a></li>
                            <li><a href="index.php?option=Page4">PAGE4</a></li>
                        </ul>
                    </div>
        </div>
            <!-- content -->
        <div class="content">	
		<div class="container">
    <div class="row">
      <div class="col-sm-6 col-sm-offset-2">
        <!-- Контейнер, содержащий форму обратной связи -->
        <div class="panel panel-info">
         
          <!-- Содержимое контейнера -->
          <div class="panel-body">

            <!-- Сообщение, отображаемое в случае успешной отправки данных -->
            <div class="alert alert-success hidden" role="alert" id="successMessage">
              <strong>Внимание!</strong> Ваше сообщение успешно отправлено.
            </div>

            <!-- Форма обратной связи -->
            <form id="contactForm">
              <div class="row">

                
                <div id="error" class="col-sm-12" style="color: #ff0000; margin-top: 5px; margin-bottom: 5px;"></div>
                
                <!-- Имя и email пользователя -->                
                <div class="col-sm-6">
                  <!-- Имя пользователя -->
                  <div class="form-group has-feedback">
                    <label for="name" class="control-label">Введите ваше имя:</label>
                    <input type="text" id="name" name="name" class="form-control" required="required" value="" placeholder="Например, Иван Иванович" minlength="2" maxlength="30">
                    <span class="glyphicon form-control-feedback"></span>
                  </div>
                </div>
                <div class="col-sm-6">
                  <!-- Email пользователя -->
                  <div class="form-group has-feedback">
                    <label for="email" class="control-label">Введите адрес email:</label>
                    <input type="email" id="email" name="email" class="form-control" required="required"  value="" placeholder="Например, ivan@mail.ru" maxlength="30">
                    <span class="glyphicon form-control-feedback"></span>
                  </div>
                </div>
              </div>

              <!-- Сообщение пользователя -->
              <div class="form-group has-feedback">
                <label for="message" class="control-label">Введите сообщение:</label>
                <textarea id="message" class="form-control" rows="3" placeholder="Введите сообщение от 20 до 500 символов" minlength="20" maxlength="500" required="required"></textarea>
              </div>

              <hr>
              <!-- Изображение, содержащее код капчи -->		  
              <img id="img-captcha" src="views/captcha.php">
              <!-- Элемент, обновляющий код капчи -->
	            <div id="reload-captcha" class="btn btn-default"><i class="glyphicon glyphicon-refresh"></i> Обновить</div>
	            <!-- Блок для ввода кода капчи -->
	            <div class="form-group has-feedback">
                <label id="label-captcha" for="captcha" class="control-label">Пожалуйста, введите указанный на изображении код:</label>
	              <input id="text-captcha" name="captcha" type="text" class="form-control" required="required" value="" autocomplete="off" minlength="6" maxlength="6">
	              <span class="glyphicon form-control-feedback"></span>
              </div>

              <!-- Кнопка, отправляющая форму -->  
              <button type="submit" class="btn btn-primary pull-right">Отправить сообщение</button>
            </form><!-- Конец формы -->

          </div>
        </div><!-- Конец контейнера -->

      </div>
    </div>
  </div>
        </div>
            
            <!-- footer --> 
        <div class="footer">
            <div class="col-xs-3">
                <p>Copyright 2016 </p>
            </div>
            <div class="col-xs-9">
                
            </div>
	</div>          
  </div>           
 </div>       
        
        
        
        
        
        
        
         
        
         <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="views/js/jquery-3.1.0.min.js"></script>
        <script src="views/js/bootstrap.min.js"></script>
        <script src="views/js/script.js"></script>
    </body>
</html>
