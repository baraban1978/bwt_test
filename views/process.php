<?php
session_start();
// переменная в которую будем сохранять результат работы
$data['result']='error';

// функция для проверки длины строки
function validStringLength($string,$min,$max) {
  $length = mb_strlen($string,'UTF-8');
  if (($length<$min) || ($length>$max)) {
    return false;
  }
  else {
    return true;
  }
}

// если данные были отправлены методом POST, то...
if (filter_input(INPUT_SERVER, 'REQUEST_METHOD') === 'POST') {
    // устанавливаем результат, равный success
    $data['result']='success';
    //получить имя, которое ввёл пользователь
    if (filter_input(INPUT_POST,'name')) {
      $name = htmlspecialchars(filter_input(INPUT_POST,'name'));
      if (!validStringLength($name,2,30)) {
        $data['name']='Поля имя содержит недопустимое количество символов.';   
        $data['result']='error';     
      }
    } else {
      $data['result']='error';
    } 
    //получить email, которое ввёл пользователь
    if (filter_input(INPUT_POST,'email')) {
      $email = htmlspecialchars(filter_input(INPUT_POST,'email'));
      if (!filter_var($email,FILTER_VALIDATE_EMAIL)) {
        $data['email']='Поле email введено неправильно';
        $data['result']='error';
      }
    } else {
      $data['result']='error';
    }
     //получить сообщение, которое ввёл пользователь
    if (filter_input(INPUT_POST,'message')) {
      $message = htmlspecialchars(filter_input(INPUT_POST,'message'));
      if (!validStringLength($message,5,500)) {
        $data['message']='Поле сообщение содержит недопустимое количество символов.';     
        $data['result']='error';   
      }      
    } else {
      $data['result']='error';
    } 
    //получить капчу, которую ввёл пользователь
    if (filter_input(INPUT_POST,'captcha')) {
      $captcha = filter_input(INPUT_POST,'captcha');
    } else {
      $data['result']='error';
    }
    // если не существует ни одной ошибки, то прододжаем... 
    if ($data['result']=='success') {
      //если пользователь правильно ввёл капчу
      if ($_SESSION["code"] != $captcha) {
        // пользователь ввёл неправильную капчу
        $data['result']='invalidCaptcha';
      }
    }
  } else {
    //данные не были отправлены методом пост
    $data['result']='error';
  }    
 
  // дальнейшие действия (ошибок не обнаружено)
  if ($data['result']=='success') {

//    //1. Сохрание формы в файл
//    $output = "---------------------------------" . "\n";
//    $output .= date("d-m-Y H:i:s") . "\n";
//    $output .= "Имя пользователя: " . $name . "\n";
//    $output .= "Адрес email: " . $email . "\n";
//    $output .= "Сообщение: " . $message . "\n";
//    if (file_put_contents(dirname(__FILE__).'/message.txt', $output, FILE_APPEND | LOCK_EX)) {
//      $data['result']='success';
//    } //else {
//      //$data['result']='error';         
//    //} 
//
//    //2. Отправляем на почту
//    // включить файл PHPMailerAutoload.php
//    require_once dirname(__FILE__) . '/phpmailer/PHPMailerAutoload.php';
//    //формируем тело письма
//    $output = "Дата: " . date("d-m-Y H:i") . "\n";
//    $output .= "Имя пользователя: " . $name . "\n";
//    $output .= "Адрес email: " . $email . "\n";
//    $output .= "Сообщение: " . "\n" . $message . "\n";
//
//    // создаём экземпляр класса PHPMailer
//    $mail = new PHPMailer;
//
//    $mail->CharSet = 'UTF-8'; 
//    $mail->From      = 'kb_sk@mail.ru';
//    $mail->FromName  = 'Имя сайта';
//    $mail->Subject   = 'Сообщение с формы обратной связи';
//    $mail->Body      = $output;
//    $mail->AddAddress( '380953384849@ukr.net' );
//
//    // отправляем письмо
//    if ($mail->Send()) {
//      $data['result']='success';
//    } //else {
//      //$data['result']='error';
//    //}      
    //3. Записываем в БД
    
       require_once("../config.php");
    ///Соединямся с БД
    $db_con = mysql_connect(DBHOST, DBUSER, DBPASS) or die("Не удалось подключиться: " . mysql_error());
    mysql_select_db(DB) or die("Нет такой базы данных");
    
    mysql_query("set character_set_client='utf8'");
    mysql_query("set character_set_results='utf8'");
    mysql_query("set collation_connection='utf8_general_ci'");
                    
    $string_query = "INSERT INTO feedback SET name='".mysql_real_escape_string($name).
                "', email='".mysql_real_escape_string($email).
                 "', text='".mysql_real_escape_string($message).
                "'";
    mysql_query($string_query);
    mysql_close($db_con);
    $data['result']='success';
    
  }
  
  // формируем ответ, который отправим клиенту
  echo json_encode($data);
?>