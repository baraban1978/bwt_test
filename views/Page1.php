<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Quest - bwt</title>

    <!-- Bootstrap -->
    <link href="views/css/bootstrap.min.css" rel="stylesheet">
    <link href="views/css/my_style.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
    <body>
        <div class="karkaswrapper">
        <div class="karkas">
            <!-- heder -->
        <div class="header">
            <div class="col-xs-3 logo">
                <a href="http://www.groupbwt.com/quest/">
                    <img src="views/img/logo1.png" alt="логотип" title="logo"
                        onmouseup="this.src='views/img/logo2.png'" 
                        onmousedown="this.src='views/img/logo1.png'"
			onmouseout="this.src='views/img/logo1.png'" 
                        onmouseover="this.src='views/img/logo2.png'">
                </a>
            </div>
            <div class="col-xs-6 page_name">
                <p>PAGE 1</p>
                <p>Форма регистрации пользователей</p>
                
            </div>
            <div class="col-xs-3">
            <div class="pass_form">
                <form action="views/php/login.php" method="post" id="form1"
                      accept-charset="" <?php if (isset($_COOKIE['id'])){echo 'hidden="hidden"';} ?>>
                   <label class="control-label input-sm"> Email: </label>
                   <input type="text" class="form-control input-sm" name="user" />
                   <br />
                   <label class="control-label input-sm"> Пароль: </label>
                   <input type="password" class="form-control input-sm" name="pass" />
                   <br />
                   <input type="submit" class="btn btn-danger btn-xs" name="submit1" value="Войти" id="my_botton"/>
                </form>
                <form action="views/php/logout.php" method="post" <?php if (isset($_COOKIE['id'])){}else{echo 'hidden="hidden"';}?>>
                   <?php
                   echo '<pre>';
                   echo 'Привет';
                   echo '<br>';
                    print_r($_COOKIE['name']);
                   echo '<br>'; 
                    print_r($_COOKIE['surname']);
                   echo '</pre>';
                   ?>
                   <input type="submit" class="btn btn-danger btn-xs" name="submit2" value="Выход"/>
                </form>
            </div>
            </div>
	</div>
            <!-- menu -->
        <div class="menu_wrapper">
                    <div class="menu-main">
                        <ul class="menu">
                            <li><a href="index.php?option=Page1">PAGE1</a></li>
                            <li><a href="index.php?option=Page2">PAGE2</a></li>
                            <li><a href="index.php?option=Page3">PAGE3</a></li>
                            <li><a href="index.php?option=Page4">PAGE4</a></li>
                        </ul>
                    </div>
        </div>
            <!-- content -->
        <div class="content">	
            <form action="views/php/register.php" class="form-horizontal" method="post" id="form" <?php if (isset($_COOKIE['id'])){echo 'hidden="hidden"';} ?>>
 
		<div class="form-group">
			<label class="control-label col-xs-3" for="firstName">Имя:<span class="red">*</span></label>
			<div class="col-xs-6">
                            <input type="text" class="form-control" name="firstName" id="firstName" placeholder="Введите имя" required>
                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                           
			</div>
			<label class="control-label col-xs-3"></label>
		</div>
		<div class="form-group">
			<label class="control-label col-xs-3" for="lastName">Фамилия:<span class="red">*</span></label>
			<div class="col-xs-6">
				<input type="text" class="form-control" name="lastName" id="lastName" placeholder="Введите фамилию" required>
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
			</div>
			<label class="control-label col-xs-3"></label>
		</div>
		<div class="form-group">
			<label class="control-label col-xs-3">Дата рождения:</label>
			<div class="col-xs-2">
				<select class="form-control" name="day">
                                    <option value="01">01</option>
                                    <option value="02">02</option>
                                    <option value="03">03</option>
                                    <option value="04">04</option>
                                    <option value="05">05</option>
                                    <option value="06">06</option>
                                    <option value="07">07</option>
                                    <option value="08">08</option>
                                    <option value="09">09</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12">12</option>
                                    <option value="13">13</option>
                                    <option value="14">14</option>
                                    <option value="15">15</option>
                                    <option value="16">16</option>
                                    <option value="17">17</option>
                                    <option value="18">18</option>
                                    <option value="19">19</option>
                                    <option value="20">20</option>
                                    <option value="21">21</option>
                                    <option value="22">22</option>
                                    <option value="23">23</option>
                                    <option value="24">24</option>
                                    <option value="25">25</option>
                                    <option value="26">26</option>
                                    <option value="27">27</option>
                                    <option value="28">28</option>
                                    <option value="29">29</option>
                                    <option value="30">30</option>
                                    <option value="31">31</option>
				</select>
			</div>
			<div class="col-xs-2">
				<select class="form-control" name="mon">
                                    <option value="01">Января</option>
                                    <option value="02">Февраля</option>
                                    <option value="03">Марта</option>
                                    <option value="04">Апреля</option>
                                    <option value="05">Мая</option>
                                    <option value="06">Июня</option>
                                    <option value="07">Июля</option>
                                    <option value="08">Августа</option>
                                    <option value="09">Сентября</option>
                                    <option value="10">Октября</option>
                                    <option value="11">Ноября</option>
                                    <option value="12">Декабря</option>
				</select>
			</div>
			<div class="col-xs-2">
                            <select class="form-control" name="year">
<?php
$year = 1950;
for ($i = 0; $i <= 60; $i++) // Цикл от 0 до 60
{
  $new_years = $year + $i; // Формируем новое значение
  echo '<option value='.$new_years.'>'.$new_years.'</option>'; //Формируем новую строчку
}
?> 

				</select>
			</div>
			<label class="control-label col-xs-3"></label>
		</div>
		<div class="form-group">
			<label class="control-label col-xs-3" for="inputEmail">Email:<span class="red">*</span></label>
			<div class="col-xs-6">
				<input type="email" name="email" class="form-control" id="inputEmail" placeholder="Email" required>
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
			</div>
			<label class="control-label col-xs-3"></label>
		</div>
                <div class="form-group">
                        <label class="control-label col-xs-3" for="inputPassword">Пароль:<span class="red">*</span></label>
                        <div class="col-xs-6">
                            <input type="password" name="password" class="form-control" id="inputPassword" placeholder="Введите пароль" required>
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                        </div>
                        <label class="control-label col-xs-3"></label>
                </div>
                <div class="form-group">
                        <label class="control-label col-xs-3">Пол:</label>
                        <div class="col-xs-2">
                                <label class="radio-inline">
                                    <input type="radio" name="sex" value="1"  checked="checked"> Мужской
                                </label>
                        </div>
                        <div class="col-xs-2">
                                <label class="radio-inline">
                                    <input type="radio" name="sex" value="0"> Женский
                                </label>
                        </div>
                </div>
                <br />
                <div class="form-group">
                        <div class="col-xs-offset-3 col-xs-9">
                                <input type="submit" name="submit" class="btn btn-danger" value="Регистрация">
                                <input type="reset" class="btn btn-default" value="Очистить форму">
                        </div>
                </div>
  
                </form>
                <form class="form-horizontal" <?php if (isset($_COOKIE['id'])){}else{echo 'hidden="hidden"';}?>>
                    <label class="col-xs-3">
                        </label>
                         <label class="col-xs-6">
                             <h1> Добро пожаловать!!!</h1>
                             <h2> Вы вошли на мой тестовый сайт :-)</h2>
                             <h3> Теперь Вам доступны страницы</h3>
                             <h3> Page2, Page3, Page4,</h3>
                             <h4> Время доступа одной регистрации 1 час!!!</h4>
                             
                         </label>
                    <label class="col-xs-3">
                        </label>
                </form>
        </div>
            
            <!-- footer --> 
        <div class="footer">
            <div class="col-xs-3">
                <p>Copyright 2016 </p>
            </div>
            <div class="col-xs-9">
                
            </div>
	</div>          
  </div>           
 </div>       
        
        
        
        
        
        
        
         
        
         <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="views/js/bootstrap.min.js"></script>
        <script src="views/js/validator.min.js"></script>
        <script>
$(function(){
	$('#form').validator({
		feedback: {
			success: 'glyphicon-thumbs-up',
			error: 'glyphicon-thumbs-down'
		}
	});
});
	</script>
        
    </body>
</html>
