<?php
///Используется для отправки необработанных HTTP-шапок
///В частности для установки кодировки документа
header("Content-Type:text/html;charset=UTF-8");

///Задает, какие ошибки PHP попадут в отчет.  Добавлять в отчет все PHP ошибки
error_reporting(E_ALL);
///Эта настройка определяет, требуется ли выводить ошибки на экран вместе с остальным выводом, 
//либо ошибки должны быть скрыты от пользователя. 
ini_set('display_errors', 1); 

///Загружаю файл конфигурации содержащий данные для подключения БД bwt_quest
require_once("config.php");
$db_con = mysql_connect(DBHOST, DBUSER, DBPASS) or die("Не удалось подключиться: " . mysql_error());
mysql_select_db(DB) or die("Нет такой базы данных");

/// Если в массиве GET существует ячейка 'option'
///$value = filter_input(INPUT_GET, 'option', FILTER_VALIDATE_URL);

$value = filter_input(INPUT_GET, 'option');
///echo $value;
if ($value) {
    // существует
    // присваиваем переменной $my_page значение 'option'
    // прогоняем значение через функции
    // strip_tags - Удаляет HTML и PHP тэги из строки
    // trim - Удаляет пробелы из начала и конца строки 
	$my_page = trim(strip_tags($value));
}
else {
    // не существует или запрос неправильный отправляем на главную (стартовую) страницу
	$my_page = 'Page1';
}
///echo ("views/".$my_page.".php");
//echo '<pre>';
//print_r($_COOKIE);
//echo '</pre>';
//print_r($_COOKIE['id']);
if (isset($_COOKIE['id'])){
        $string_query = "SELECT * FROM users WHERE id='".mysql_real_escape_string($_COOKIE['id'])."'";
        $query  = mysql_query($string_query);
        if (mysql_result($query, 0, 'user_hash') == $_COOKIE['hash'])
            {}
            else {
                setcookie("id", '', time()-3600, '/');
                mysql_close($db_con);
                header("Location: ../../index.php"); exit();
            }
}
 else {
        $my_page = 'Page1';
}

/// Проверяем существует ли файл с таким именем и загружаем его.
if(file_exists("views/".$my_page.".php")) {
	/// загружаем основной шаблон
	include("views/".$my_page.".php");
	
}
else {
	exit("<p>Не правильный адресс</p>");
}


